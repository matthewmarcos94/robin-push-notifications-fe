self.addEventListener('push', function(event) {
  console.log(`[Service Worker] Push received with this data: "${event.data.text()}"`);
  const { sender, message } = JSON.parse(event.data.text());
  const title = 'Robin API';
  const options = {
    body: `User #${sender}: "${message}"`,
  };

  event.waitUntil(self.registration.showNotification(title, options));
});


self.addEventListener('notificationclick', function(event) {
  console.log('[Service Worker] Notification click Received.');
  event.notification.close();
  event.waitUntil(
    clients.openWindow('https://developers.google.com/web/')
  );
});
