// Do event listeners for the messagebox
const $ = jQuery;
const BASE_URL = 'http://localhost:5000/api'
const VAPID_KEY_URL = '/notifications/vapid_key'
const SEND_MESSAGE_URL = '/messages'
let swRegistration;  // Service Worker

console.log("Loaded JS!")

function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}


$(function() {
  function subscribeUser() {
    const applicationServerPublicKey = localStorage.getItem('vapid_key');
    const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
    swRegistration.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: applicationServerKey
      })
      .then(function(subscription) {
        console.log('User is subscribed.');

        // updateSubscriptionOnServer(subscription);
        localStorage.setItem('sub_token',JSON.stringify(subscription));
        isSubscribed = true;
      })
      .catch(function(err) {
        console.log('Failed to subscribe the user: ', err);
      });
  }

  /**
   * Send message to backend!
   */
  $('#button').on('click', function(ev) {
    const $form = $('#form');
    const $sendTo = $('#send_to');
    const $conversationId = $('#conversation_id');
    const $sender = $('#sender');
    const $message = $('#message');

    const sendTo = Number($sendTo.val());
    const sender = Number($sender.val());
    const conversationId = Number($conversationId.val());
    const message = $message.val();

    if(!(sendTo && sender && message)) {
      // Form check lol
      console.log('Whoops! Fill it out!')
      return;
    }

    $.ajax({
      url: BASE_URL + '/messages',
      headers: {
        'Content-Type':'application/json'
      },
      method: 'POST',
      dataType: 'json',
      data: JSON.stringify({
        message,
        links: 'FROM FE APP',
        message_type: 'Random type',
        conversation_id: conversationId,
        receiver_id: sendTo,
        sender_id: sender,
        'sub_token': localStorage.getItem('sub_token'),
      }),
      success: function(data) {
        console.log('Successfully sent message')
        console.log(data);
      },
      error: function(err) {
        console.log('Error!');
        console.error(err);
      }
    });


  });

  // GET Vapid key
  $.ajax({
    type:'GET',
    url: BASE_URL + VAPID_KEY_URL,
    success:function(response){
      console.log('response',response);
      const { data } = response;
      localStorage.setItem('vapid_key', data.vapid_key);
      subscribeUser();
    }
  });
});


// Register service workers
$(function() {
  if ('serviceWorker' in navigator && 'PushManager' in window) {
    console.log('Service Worker and Push is supported');

    navigator.serviceWorker.register('message_notification_sw.js')
      .then(function(swReg) {
        console.log('Service Worker is registered', swReg);

        swRegistration = swReg;
      })
      .catch(function(error) {
        console.error('Service Worker Error', error);
      });
  } else {
    console.warn('Push Messaging is not supported');
    pushButton.textContent = 'Push Not Supported';
  }
});
